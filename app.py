from datetime import datetime
from flask import Flask, request, render_template, jsonify
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy
from marshmallow import Schema, fields, ValidationError, pre_load
import os

basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    SECRET_KEY = os.environ.get('SECRET_KEY')
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') \
        or 'sqlite:///' + os.path.join(basedir, 'app.db')
    SQLALCHEMY_TRACK_MODIFICATIONS = False

app = Flask(__name__)
app.config.from_object(Config)
db = SQLAlchemy(app)
migrate = Migrate(app, db)


# Models
class Team(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(254), index=True)
    logo = db.Column(db.String(254))


class Survey(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(254), index=True)
    status = db.Column(db.String(8))
    team_id = db.Column(db.Integer, db.ForeignKey('team.id'))


class Response(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    score = db.Column(db.Float)
    timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    survey_id = db.Column(db.Integer, db.ForeignKey('survey.id'))


# Schemas
class TeamSchema(Schema):
    id = fields.Int(dump_only=True)
    name = fields.String(required=True)
    logo = fields.String(missing='images/logo.png')


class SurveySchema(Schema):
    id = fields.Int(dump_only=True)
    name = fields.String(required=True)
    status = fields.String(missing='open')
    team_id = fields.Int(required=True)


class ResponseSchema(Schema):
    id = fields.Int(dump_only=True)
    score = fields.Float(required=True)
    timestamp = fields.DateTime(dump_only=True)
    survey_id = fields.Int(required=True)

    @pre_load
    def process_score(self, data):
        sum = 0
        for i in data:
            sum += float(data.get(i))
        data['score'] = sum / len(data)
        return data

questions = ['Question 1', 'Question 2', 'Question 3']


@app.route('/', methods=["GET", "POST"])
def index():
    return render_template('index.html', questions=questions)


@app.route('/api/v1/questions', methods=["GET"])
def get_questions():
    return jsonify(questions)


@app.route('/api/v1/team', methods=["GET"])
def get_teams():
    teams = Team().query.all()
    teams_schema = TeamSchema(many=True)
    result = teams_schema.dumps(teams, many=True)
    return result


@app.route('/api/v1/team/<int:id>', methods=["GET"])
def get_team(id):
    team = Team.query.get(id)
    team_schema = TeamSchema()
    result = team_schema.dumps(team)
    return result


@app.route('/api/v1/team', methods=["POST"])
def new_team():
    json_data = request.get_json()
    team_schema = TeamSchema()
    if not json_data:
        return jsonify({'messsage': 'No input provided'}), 400
    try:
        data, errors = team_schema.load(json_data)
    except ValidationError as err:
        return jsonify(err.messages), 422

    name, logo = data['name'], data['logo']
    new_team = Team(name=name, logo=logo)
    db.session.add(new_team)
    db.session.commit()
    return team_schema.dumps(new_team)


@app.route('/api/v1/team/<int:id>', methods=["PUT"])
def update_team(id):
    json_data = request.get_json()
    team_schema = TeamSchema()
    if not json_data:
        return jsonify({'message': 'No input provided'}), 400
    try:
        data, errors = team_schema.load(json_data)
    except ValidationError as err:
        return jsonify(err.messages), 422

    t = Team.query.get(id)
    t.name, t.logo = data['name'], data['logo']
    db.session.commit()
    return team_schema.dumps(t)


@app.route('/api/v1/team/<int:id>', methods=["DELETE"])
def delete_team(id):
    team = Team.query.get(id)
    db.session.delete(team)
    db.session.commit
    return jsonify({'message': 'Deleted'})


@app.route('/api/v1/survey', methods=["GET"])
def get_surveys():
    surveys = Survey().query.all()
    surveys_schema = SurveySchema(many=True)
    result = surveys_schema.dumps(surveys, many=True)
    return result


@app.route('/api/v1/survey/<int:id>', methods=["GET"])
def get_survey(id):
    survey = Survey.query.get(id)
    survey_schema = SurveySchema()
    result = survey_schema.dumps(survey)
    return result


@app.route('/api/v1/survey', methods=["POST"])
def new_survey():
    json_data = request.get_json()
    survey_schema = SurveySchema()
    if not json_data:
        return jsonify({'message': 'No input provided'}), 400
    try:
        data, errors = survey_schema.load(json_data)
    except ValidationError as err:
        return jsonify(err.messages), 422

    name, team_id = data['name'], data['team_id']
    new_survey = Survey(name=name, team_id=team_id)
    db.session.add(new_survey)
    db.session.commit()
    return survey_schema.dumps(new_survey)


@app.route('/api/v1/survey/<int:id>', methods=["PUT"])
def update_survey(id):
    json_data = request.get_json()
    survey_schema = SurveySchema()
    if not json_data:
        return jsonify({'message': 'No input provided'}), 400
    try:
        data, errors = survey_schema.load(json_data)
    except ValidationError as err:
        return jsonify(err.messages), 422

    s = Survey.query.get(id)
    s.name, s.team_id, s.status = data['name'], data['team_id'], data['status']
    db.session.commit()
    return survey_schema.dumps(s)


@app.route('/api/v1/survey/<int:id>', methods=["DELETE"])
def delete_survey(id):
    survey = Survey.query.get(id)
    db.session.delete(survey)
    db.session.commit()
    return jsonify({'message': 'Deleted'})


@app.route('/api/v1/response', methods=["GET"])
def get_responses():
    responses = Response().query.all()
    responses_schema = ResponseSchema(many=True)
    result = responses_schema.dumps(responses, many=True)
    return result


@app.route('/api/v1/response/<int:id>', methods=["GET"])
def get_response(id):
    response = Response.query.get(id)
    response_schema = ResponseSchema()
    result = response_schema.dumps(response)
    return result


@app.route('/api/v1/response', methods=["POST"])
def new_response():
    json_data = request.get_json()
    response_schema = ResponseSchema()
    if not json_data:
        return jsonify({'message': 'No input provided'}), 400
    try:
        data, errors = response_schema.load(json_data)
    except ValidationError as err:
        return jsonify(err.messages), 422

    response = Response(score=data['score'], survey_id=data['survey_id'])
    db.session.add(response)
    db.session.commit()
    return response_schema.dumps(response)


@app.route('/api/v1/response/<int:id>', methods=["DELETE"])
def delete_response(id):
    response = Response.query.get(id)
    db.session.delete(response)
    db.session.commit()
    return jsonify({'message': 'Deleted'})

if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0")
