FROM python:3

WORKDIR /app

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

ENV FLASK_APP app.py
COPY . /app

EXPOSE 5000

ENTRYPOINT ["python"]
CMD ["app.py"]
