# TopMorale Team Happiness

## Installing requirements

```shell
pip install --no-cache-dir -r requirements.txt
```

## Initial setup

```shell
export FLASK_APP=app.py
flask db init
flask db migrate -m 'response table'
flask db upgrade
```